json.extract! issue, :id, :subject, :text, :user, :repository_id, :created_at, :updated_at
json.url issue_url(issue, format: :json)
