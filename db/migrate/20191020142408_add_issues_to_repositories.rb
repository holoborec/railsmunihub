class AddIssuesToRepositories < ActiveRecord::Migration[6.0]
  def change
    add_reference :repositories, :issue, foreign_key: true
    add_foreign_key :issues, :repositories
  end
end
