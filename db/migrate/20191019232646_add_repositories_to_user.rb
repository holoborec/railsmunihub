class AddRepositoriesToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :repository
    add_foreign_key :repositories, :users
  end

end  
