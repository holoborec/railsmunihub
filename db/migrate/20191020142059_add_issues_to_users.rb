class AddIssuesToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :issue, foreign_key: true
    add_foreign_key :issues, :users
  end
end
