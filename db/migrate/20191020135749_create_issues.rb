class CreateIssues < ActiveRecord::Migration[6.0]
  def change
    create_table :issues do |t|
      t.string :subject
      t.string :text
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :repository, null: false, foreign_key: true

      t.timestamps
    end
  end
end
